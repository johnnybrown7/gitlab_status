require "gitlab_status/version"
require "net/http"
require "date"

module GitlabStatus
  GITLAB_URI = "https://gitlab.com/"

  def GitlabStatus.fetch(uri_str, limit = 10)

    raise Net::HTTPError, 'too many HTTP redirects' if limit == 0

    response = Net::HTTP.get_response(URI(uri_str))

    case response
    when Net::HTTPSuccess then
      response
    when Net::HTTPRedirection then
      location = response['location']
      fetch(location, limit - 1)
    else
      response.value
    end
  end


  def GitlabStatus.status(follow_redirect = false)
    start = DateTime.now
    if follow_redirect
      response = fetch(GITLAB_URI)
    else
      response = Net::HTTP.get_response(URI(GITLAB_URI))
    end
    response
    Float(DateTime.now - start) * 24*60*60
  end
end
