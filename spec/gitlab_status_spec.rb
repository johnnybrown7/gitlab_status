require 'spec_helper'

describe GitlabStatus do
  it 'has a version number' do
    expect(GitlabStatus::VERSION).not_to be nil
  end

  it 'returns the response time' do
    stub_request :any, "https://gitlab.com/"
    s = GitlabStatus.status
    expect(s).to be_instance_of(Float)
  end

  it 'handles redirects when specified' do
    stub_request(:get, "https://gitlab.com/").
      to_return(status: 302, headers: {"location": "https://about.gitlab.com/"})
    stub_request(:get, "https://about.gitlab.com/")

    s = GitlabStatus.status(folow_redirect=true)
    expect(s).to be_instance_of(Float)
  end
end
