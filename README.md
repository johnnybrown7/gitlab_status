# GitlabStatus

Queries https://gitlab.com and prints the response time

## Installation

    gem build gitlab_status.gemspec
    sudo gem install gitlab_status-0.1.0.gem

## Usage

    gitlab_status

To see the response time including all redirects, execute

    gitlab_status -r

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

